import sys

### Very usefull comment that solves everything

def collatz_len(N):
    return 1, 1

if __name__ == "__main__":
    # should really use argparse here
    if len(sys.argv) < 2:
        print(f"Usage: {__file__} NNN")
        exit(1)

    max_N = int(sys.argv[1])
    start, length = collatz_len(max_N)
    print(f"The longest collatz chain up to {N} starts at {start} and has {length} elements.")
